import React, { useState } from "react";
import { Form ,Button } from 'react-bootstrap';


const AddTodo =({addTodo}) => {

    const [title,setTitle] = useState('')
    const [description,setDescription] = useState('')

    const addTodoHandler = (e) => {
        e.preventDefault()
        addTodo({
            title : title,
            description : description,
            completed: false,
        })
    }
    return(
       <Form>
           <Form.Group className="mb-3" controlID='title'>
               <Form.Label>Title</Form.Label>
               <Form.Control type="text" placeholder="Enter Todo Title" onChange={e => setTitle(e.target.value)}></Form.Control>
               <Form.Text className="text-muted">
                   Please stick to your words
               </Form.Text>
           </Form.Group>
           <Form.Group className="mb-3" controlID='description'>
               <Form.Label>Description</Form.Label>
               <Form.Control type="text" placeholder="Enter Description" onChange={e => setDescription(e.target.value)}></Form.Control>
               <Form.Text className="text-muted">
                   Please make it short!!!
               </Form.Text>
           </Form.Group>
           <Button variant="outline-primary" type="submit" onClick={addTodoHandler} >Add Todo</Button>
       </Form>
    )
};













export default AddTodo;