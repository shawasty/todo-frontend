import React from "react";
import { Row, Col, Form, Button } from "react-bootstrap";


const Todo =({ id, title, description }) =>{
    return(
        <div>
            <Row className='border-bottom pt-3'>
                <Col md={1}>
                <Form>
                   <Form.Check 
                   type="checkbox"/> 
                </Form>
                </Col>

                <Col>
                    <h5>{title}</h5>
                    <p>{description}</p>
                </Col>
                <Col md={2}>
                    <Form>
                        <Button variant='outline-info' className='my-2 btn-block'> Edit </Button>
                    </Form>
                    <Form>
                        <Button variant='outline-danger' className='my-2 btn-block'>Delete</Button>
                    </Form>
                </Col>
            </Row>
        </div>
    )
};
















export default Todo;