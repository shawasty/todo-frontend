import React,{ useEffect, useState } from 'react';
import AddTodo from './Components/AddTodo';
import Todo from './Components/Todo';
import './App.css';
import { Container, Row, Col, Card } from 'react-bootstrap';
import axios from 'axios' 


function App() {

  const [todos, setTodos] = useState([])

  const getTodos =async () =>{
    try {
      const response  = await axios.get('/api/tasks/')
      const {data} = response
      setTodos(data)

    } catch (err) {
      console.log(err)
    }
  }
  useEffect(() => {
    getTodos()
  },[])

  const addTodo =async (newTodo) => {
    try {
      console.log(newTodo)
     await axios.post('/api/tasks/', newTodo )
     getTodos()

    } catch (err) {
      console.log(err)
    }

  }

  return (
    <div className="wrapper">
    <Container>
      <Row className ="justify-content-center pt-5">
        <Col>
          <Card className='p-5'>
            <h3>My Todos</h3>
            <AddTodo addTodo ={addTodo} />
            {todos.map((todo,index) =>(
              <Todo id={todo.id} title={todo.title} description={todo.description} />
            ))}
          </Card>
        </Col>
      </Row>
      </Container>
     
    </div>

  );
}

export default App;
